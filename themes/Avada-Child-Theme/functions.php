<?php

function theme_enqueue_styles() {
		$lang = get_stylesheet_directory() . '/languages';
	load_child_theme_textdomain( 'Avada', $lang );
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'avada-stylesheet' ),time());
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles',20);

function avada_lang_setup() {

}
add_action( 'after_setup_theme', 'avada_lang_setup' );

add_filter( 'woocommerce_cart_item_permalink', '__return_null' );

function disable_shipping_calc_on_cart( $show_shipping ) {
    if( is_cart() ) {
        return false;
    }
    return $show_shipping;
}
add_filter( 'woocommerce_cart_ready_to_calc_shipping', 'disable_shipping_calc_on_cart', 99 );

add_action('wp_logout','auto_redirect_after_logout');

function auto_redirect_after_logout(){
  wp_safe_redirect( home_url() );
  exit;
}