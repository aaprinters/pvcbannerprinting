<?php
/**
 * Template for the secondary header.
 *
 * @author     ThemeFusion
 * @copyright  (c) Copyright by ThemeFusion
 * @link       http://theme-fusion.com
 * @package    Avada
 * @subpackage Core
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php

$content_1 = avada_secondary_header_content( 'header_left_content' );
$content_2 = avada_secondary_header_content( 'header_right_content' );
?>

<div class="fusion-secondary-header">
	<div class="fusion-row">
		<?php //if ( $content_1 ) : ?>
			<div class="fusion-alignleft">
				<?php //echo $content_1; // WPCS: XSS ok. ?>
				<p class="header_top_text"><strong class="header_top_label" style="color:#fff;">Free Artwork Service </strong>design@pvcbannersprinting.co.uk</p>
			</div>
		<?php //endif; ?>
		<?php //if ( $content_2 ) : ?>
			<div class="fusion-alignright">
				<?php //echo $content_2; // WPCS: XSS ok. ?>
				<div class="login_reg">
					<?php if ( is_user_logged_in() ) { ?>
					<?php global $current_user; wp_get_current_user(); echo 'Welcome: ' .$current_user->display_name; ?> | <a href="http://pvcbannersprinting.co.uk/wp-login.php?action=logout">Logout</a>
					<?php } else{ ?>
						<a href="https://pvcbannersprinting.co.uk/login/">login</a> / <a href="https://pvcbannersprinting.co.uk/register/">Register</a>
					<?php }?>	 
				</div>
				<div class="fusion-social-links header_top_social">
					<div class="fusion-social-networks">
						<div class="fusion-social-networks-wrapper">
							<a class="fusion-social-network-icon fusion-tooltip fusion-facebook fusion-icon-facebook" style="color:#bebdbd;" aria-label="fusion-facebook" target="_blank" rel="noopener noreferrer" href="https://business.facebook.com/Pvcbannersprinting/?business_id=738299013605459" data-placement="top" data-title="Facebook" data-toggle="tooltip" title="" data-original-title="Facebook"></a>
							<a class="fusion-social-network-icon fusion-tooltip fusion-linkedin fusion-icon-linkedin" style="color:#bebdbd;" aria-label="fusion-linkedin" target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/company/65761974/admin/" data-placement="top" data-title="Linkedin" data-toggle="tooltip" title="" data-original-title="Linkedin"></a>
							<a class="fusion-social-network-icon fusion-tooltip fusion-instagram fusion-icon-instagram" style="color:#bebdbd;" aria-label="fusion-instagram" target="_blank" rel="noopener noreferrer" href="https://www.instagram.com/pvcbannersprintinguk/" data-placement="top" data-title="Instagram" data-toggle="tooltip" title="" data-original-title="Instagram"></a>
							<a class="fusion-social-network-icon fusion-tooltip fusion-twitter fusion-icon-twitter" style="color:#bebdbd;" aria-label="fusion-twitter" target="_blank" rel="noopener noreferrer" href="https://twitter.com/banner_pvc" data-placement="top" data-title="Twitter" data-toggle="tooltip" title="" data-original-title="Twitter"></a>
						</div>
					</div>
				</div>
				

			</div>
		<?php //endif; ?>
	</div>
</div>
