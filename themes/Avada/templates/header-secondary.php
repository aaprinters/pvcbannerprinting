<?php
/**
 * Template for the secondary header.
 *
 * @author     ThemeFusion
 * @copyright  (c) Copyright by ThemeFusion
 * @link       http://theme-fusion.com
 * @package    Avada
 * @subpackage Core
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php

$content_1 = avada_secondary_header_content( 'header_left_content' );
$content_2 = avada_secondary_header_content( 'header_right_content' );
?>

<div class="fusion-secondary-header">
	<div class="fusion-row">
		<?php //if ( $content_1 ) : ?>
			<div class="fusion-alignleft">
				<?php //echo $content_1; // WPCS: XSS ok. ?>
				<p class="header_top_text">Free <strong>Artwork</strong> Service design@pvcbannersprinting.co.uk</p>
			</div>
		<?php //endif; ?>
		<?php //if ( $content_2 ) : ?>
			<div class="fusion-alignright">
				<?php //echo $content_2; // WPCS: XSS ok. ?>
				<?php wp_nav_menu( array(
					    'menu'           => 'Cart Menu', // Do not fall back to first non-empty menu.
					    'theme_location' => '__no_such_location',
					    'fallback_cb'    => false // Do not fall back to wp_page_menu()
					) ); ?>
				<div class="login_reg">
					<?php if ( is_user_logged_in() ) { ?>
					<?php echo 'Welcome: ' .$current_user->display_name; ?>
					<?php } else{ ?>
						<a href="https://pvcbannersprinting.co.uk/login/">login</a> / <a href="https://pvcbannersprinting.co.uk/register/">Register</a>
					<?php }?>	 
				</div>
					
			</div>
		<?php //endif; ?>
	</div>
</div>
